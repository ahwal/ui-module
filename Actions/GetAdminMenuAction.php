<?php

namespace Modules\UI\Actions;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Common\Actions\GetAhwalConfigs;
use Modules\UI\Navigation\NavigationItem;

class GetAdminMenuAction
{
    public static function run()
    {
        return collect(GetAhwalConfigs::run('menu.admin', []))
            ->sortBy('position')
            ->map(fn ($item) => static::buildItem($item))
            ->filter()
            ->toArray();
    }

    private static function buildItem(array $item): ?NavigationItem
    {
        if (isset($item['permission']) && Auth::user()->cannot($item['permission'])) {
            return null;
        }

        return NavigationItem::make()
            ->label($item['label'] ?? null)
            ->route($item['route'] ?? null)
            ->link($item['link'] ?? null)
            ->submenu(!empty($item['children'])
                ? collect($item['children'])
                    ->map(fn ($item) => static::buildItem($item))
                    ->filter()
                    ->toArray()
                : null
            )
            ->isActiveWhen(function (NavigationItem $i) {
                if ($i->getSubmenu()) {
                    foreach ($i->getSubmenu() as $m) {
                        if ($m->isActive()) return true;
                    }
                }
                return $i->getRoute() && Route::is($i->getRoute());
            });
    }

    // TODO: fix this
    private static function filterByPermission($menu)
    {
        return collect($menu)
            ->map(function ($group) {
                if (isset($group['permission']) && Auth::user()->cannot($group['permission'])) {
                    return false;
                }

                if (empty($group['children'])) return false;

                $group['children'] = collect($group['children'])
                    ->map(function ($item) {
                        if (isset($item['permission']) && Auth::user()->cannot($item['permission'])) {
                            return false;
                        }

                        $item['link'] = Route::has($item['route']) ? route($item['route']) : $item['route'];

                        return $item;
                    })
                    ->filter()
                    ->toArray();

                if (empty($group['children'])) return false;

                return $group;
            })
            ->filter()
            ->sortBy('priority')
            ->toArray();
    }
}
