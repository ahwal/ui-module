<?php

namespace Modules\UI\Actions;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Common\Actions\GetAhwalConfigs;

class GetPortalMenuAction
{
    public static function run()
    {
        $items = collect(GetAhwalConfigs::run('menu.portal', []))
            ->sortBy('position')->toArray();

        return self::filterByPermission($items);
    }

    private static function filterByPermission($menu)
    {
        return collect($menu)
            ->map(function ($group) {
                if (isset($group['permission']) && Auth::user()->cannot($group['permission'])) {
                    return false;
                }

                if (empty($group['children'])) return false;

                $group['children'] = collect($group['children'])
                    ->map(function ($item) {
                        if (isset($item['permission']) && Auth::user()->cannot($item['permission'])) {
                            return false;
                        }

                        $item['link'] = Route::has($item['route']) ? route($item['route']) : $item['route'];

                        return $item;
                    })
                    ->filter()
                    ->toArray();

                if (empty($group['children'])) return false;

                return $group;
            })
            ->filter()
            ->sortBy('priority')
            ->toArray();
    }
}
