<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            @hasSection('page-title')
                @yield('page-title') &ndash;
            @endif
            {{ config('app.name', 'Ahwal Edu') }}
        </title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @stack('head')
        @livewireStyles

        <style>[x-cloak] {display: none}</style>

        <!-- Scripts -->
        <script>
            window.readyForAlpine = function (callback) {
                const event = typeof Alpine == 'undefined' ? 'alpine:init' : 'livewire:load';
                document.addEventListener(event, callback);
            }
        </script>

        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <livewire:common::data-hub-component />

        <x-jet-banner />

        <div class="min-h-screen bg-gray-100">

            <livewire:ui::admin-menu-component />

            <div class="xpt-12">
                <!-- Page Heading -->
                @if (isset($header))
                    <header class="bg-white shadow">
                        <div class="p-2">
                            {{ $header }}
                        </div>
                    </header>
                @endif

                <!-- Page Content -->
                <main>
                    {{ $slot }}
                </main>
            </div>
        </div>

        @stack('modals')
        <x-ui::modal.dynamic-modal />
        <x-ui::modal.confirm-modal />
        <x-ui::modal.dynamic-drawer />
        <x-ui::notification />

        @stack('scripts')
        @livewireScripts

    </body>
</html>
