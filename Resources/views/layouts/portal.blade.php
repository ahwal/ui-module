<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            @hasSection('page-title')
                @yield('page-title') &ndash;
            @endif
            {{ config('app.name', 'Ahwal Edu') }}
        </title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @stack('head')
        @livewireStyles

        <style>[x-cloak] {display: none}</style>

        <!-- Scripts -->
        <script src="https://unpkg.com/localforage@1.10.0/dist/localforage.js"></script>
        <script>
            localforage.setDriver([localforage.INDEXEDDB, localforage.LOCALSTORAGE ])
        </script>
        <script src="{{ mix('js/app.js') }}" defer></script>

    </head>
    <body class="font-sans antialiased" x-data="{menuOpen: false, menuClosable: true}">
        <livewire:common::data-hub-component />

        <div class="fixed top-0 z-10 flex items-center w-full h-10 px-2 space-x-2 text-white bg-green-600">
            <template x-if="menuClosable" hidden>
                <svg class="w-6 h-6 cursor-pointer" stroke="currentColor" fill="none" viewBox="0 0 24 24" x-on:click="menuOpen = !menuOpen">
                    <path :class="{'hidden': menuOpen, 'inline-flex': ! menuOpen }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    <path :class="{'hidden': ! menuOpen, 'inline-flex': menuOpen }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </template>

            <h1>Darusy Syahadah</h1>
        </div>
        <div x-show="menuOpen" class="absolute w-full min-h-screen pt-10 bg-white">
            <div class="max-w-lg mx-4 mt-4 sm:mx-auto">

                @can('manage.settings')
                    <x-ui::button.link href="{{ route('admin.home') }}">Akses Halaman Admin</x-ui::button.link>
                @endcan

                <nav x-data="{
                    menu: {{ json_encode(\Modules\UI\Actions\GetPortalMenuAction::run()) }}
                }" class="">

                    <template x-for="(group, index) in menu" :key="index" hidden>
                        <div class="mt-4">
                            <h2 class="py-2 font-medium" x-text="group.label"></h2>

                            <div class="grid grid-cols-3 gap-2 sm:grid-cols-4">
                                <template x-for="item in group.items" hidden>
                                    <a x-bind:href="item.link"
                                        class="flex items-center p-2 bg-white border shadow-md hover:bg-gray-100"
                                    >
                                        <div class="w-full text-center">
                                            <x-ui::svg icon="color/approval" class="w-full max-w-[32px] mx-auto" />
                                            {{-- <img x-bind:src="item.icon" class="w-full max-w-[32px] mx-auto" /> --}}
                                            <h3 class="mt-1 text-xs leading-tight" x-text="item.label"></h3>
                                        </div>
                                    </a>
                                </template>
                            </div>
                        </div>
                    </template>

                </nav>

            </div>

        </div>

        <div class="min-h-screen bg-gray-100">

            <div class="pt-10">

                <!-- Page Content -->
                <main>
                    {{ $slot }}
                </main>
            </div>
        </div>

        {{-- @stack('modals')
        <x-ui::modal.dynamic-modal />
        <x-ui::modal.confirm-modal />
        <x-ui::modal.dynamic-drawer />
        <x-ui::notification /> --}}

        @stack('scripts')
        @livewireScripts
    </body>

</html>
