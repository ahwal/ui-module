<nav
    x-data="{ open: false }"
    class="fixed top-0 z-30 w-full bg-green-500 border-b border-gray-100"
>
    <!-- Primary Navigation Menu -->
    <div class="flex justify-between h-10">
        <div class="flex">
            <div class="relative flex items-center px-3 text-white transition cursor-pointer hover:bg-green-600"
                x-data="{ open: false }"
                x-on:click="open = !open"
                x-on:click.away="open = false"
                :class="{ 'bg-green-600': open }"
            >
                <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
                <div x-cloak x-show="open"
                    x-transition:enter="transition ease-out duration-200"
                    x-transition:enter-start="transform opacity-0 scale-95"
                    x-transition:enter-end="transform opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75"
                    x-transition:leave-start="transform opacity-100 scale-100"
                    x-transition:leave-end="transform opacity-0 scale-95"
                    class="absolute left-0 z-30 w-screen origin-top-left shadow-lg sm:rounded-md sm:w-48 top-full"
                    x-on:click="open = false"
                >
                    <div class="py-1 bg-white rounded-md ring-1 ring-black ring-opacity-5">
                        @foreach ($items as $key => $item)
                            @if (!$item->isSeparator())
                                <x-ui::nav.root-item
                                    href="{{ $item->getUrl() }}"
                                    :active="session('current_module') == $key"
                                >{{ $item->getLabel() }}</x-ui::nav.root-item>
                            @else
                                <div class="border-t border-gray-100"></div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="flex items-center ml-2 text-sm font-semibold text-white sm:text-base sm:ml-4">
                <h1>{{ $title }}</h1>
            </div>

            <!-- Navigation Links -->
            <div class="hidden space-x-4 sm:ml-10 sm:flex">
                @foreach ($submenu as $item)
                <div class="relative sm:flex group">
                    @if ($item->getSubmenu())
                    <x-ui::nav.link
                        href="#"
                        x-on:click.prevent=""
                        :active="$item->isActive()"
                    >
                        {{ $item->getLabel() }}
                        <x-ui::svg icon="chevron-down" class="w-4 h-4 ml-1" />
                    </x-ui::nav.link>
                    <div class="whitespace-nowrap absolute left-0 top-full py-1 bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 hidden group-hover:block">
                        @foreach ($item->getSubmenu() as $i)
                        <x-ui::nav.root-item
                            href="{{ $i->getUrl() }}"
                            :active="$i->isActive()"
                        >
                            {{ $i->getLabel() }}
                        </x-ui::nav.root-item>
                        @endforeach
                    </div>
                    @else
                    <x-ui::nav.link
                        href="{{ $item->getUrl() }}"
                        :active="$item->isActive()"
                    >
                        {{ $item->getLabel() }}
                    </x-ui::nav.link>
                    @endif
                </div>
                @endforeach
            </div>

            <div class="flex pl-2 pr-1 ml-2 text-sm text-white sm:hidden"
                x-data="{open: false}"
                x-bind:class="{ 'bg-green-600': open }"
                x-on:click.away="open = false"
            >
                <a class="flex items-center font-light leading-none tracking-tight" href="#"
                    x-on:click="open = !open"
                >
                    @foreach ($submenu as $item)
                        @if ($item->isActive())
                            {{ $item->getLabel() }}
                            <x-ui::svg icon="chevron-down" class="w-4 h-4 ml-1 transition" x-bind:class="{'rotate-180': open}" />
                        @endif
                    @endforeach
                </a>

                <div x-cloak x-show="open"
                    x-transition:enter="transition ease-out duration-200"
                    x-transition:enter-start="transform opacity-0 scale-95"
                    x-transition:enter-end="transform opacity-100 scale-100"
                    x-transition:leave="transition ease-in duration-75"
                    x-transition:leave-start="transform opacity-100 scale-100"
                    x-transition:leave-end="transform opacity-0 scale-95"
                    class="absolute left-0 z-30 w-full origin-top shadow-lg top-full"
                >
                    <div class="py-1 bg-white ring-1 ring-black ring-opacity-5 relative">
                        @foreach ($submenu as $item)
                            @if ($item->getSubmenu())
                            <div x-data="{ open: false }">
                                <x-ui::nav.root-item
                                    href="#"
                                    :active="$item->isActive()"
                                    x-on:click.prevent="open = !open"
                                >
                                    <div class="flex items-center justify-between">
                                        {{ $item->getLabel() }}
                                        <x-ui::svg icon="chevron-right" class="w-5 h-5 transition" />
                                    </div>
                                </x-ui::nav.root-item>
                                <div
                                    class="pb-1 absolute top-0 left-0 right-0 bg-white ring-1 ring-black ring-opacity-5 shadow-lg transition"
                                    x-transition:enter="transition ease-out duration-200"
                                    x-transition:enter-start="transform opacity-0 translate-x-full"
                                    x-transition:enter-end="transform opacity-100 translate-x-0"
                                    x-transition:leave="transition ease-in duration-75"
                                    x-transition:leave-start="transform opacity-100 translate-x-0"
                                    x-transition:leave-end="transform opacity-0 translate-x-full"
                                    x-show="open"
                                >
                                    <div class="flex items-center px-4 py-3 space-x-2">
                                        <x-ui::button class="block !px-1 !py-0 mr-2" x-on:click="open = false">
                                            <x-ui::svg icon="chevron-left" class="w-5 h-5" />
                                        </x-ui::button>
                                        <h1 class="font-medium text-gray-900">
                                            {{ $item->getLabel() }}
                                        </h1>
                                    </div>
                                    @foreach ($item->getSubmenu() as $i)
                                    <x-ui::nav.root-item
                                        href="{{ $i->getUrl() }}"
                                        :active="$i->isActive()"
                                    >
                                        {{ $i->getLabel() }}
                                    </x-ui::nav.root-item>
                                    @endforeach
                                </div>
                            </div>
                            @else
                            <x-ui::nav.root-item
                                href="{{ $item->getUrl() }}"
                                :active="$item->isActive()"
                            >
                                {{ $item->getLabel() }}
                            </x-ui::nav.root-item>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="hidden sm:flex sm:items-center sm:ml-6">

            <!-- Settings Dropdown -->
            <div class="relative ml-3 mr-2">
                <x-jet-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                            <button class="flex text-xs transition border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300">
                                <img class="object-cover w-8 h-8 rounded-full" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->username }}" />
                            </button>
                        @else
                            <span class="inline-flex rounded">
                                <button type="button" class="inline-flex items-center px-2 py-1 text-xs font-medium leading-4 text-gray-500 transition bg-white border border-transparent rounded hover:text-gray-700 focus:outline-none">
                                    {{ Auth::user()->username }}

                                    <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </button>
                            </span>
                        @endif
                    </x-slot>

                    <x-slot name="content">
                        <!-- Account Management -->
                        <div class="block px-4 py-2 text-xs text-gray-400">
                            {{ __('Manage Account') }}
                        </div>

                        <x-jet-dropdown-link href="{{ route('profile.show') }}">
                            {{ __('Profile') }}
                        </x-jet-dropdown-link>

                        @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                            <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                                {{ __('API Tokens') }}
                            </x-jet-dropdown-link>
                        @endif

                        <div class="border-t border-gray-100"></div>

                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                {{ __('Log Out') }}
                            </x-jet-dropdown-link>
                        </form>
                    </x-slot>
                </x-jet-dropdown>
            </div>
        </div>

        <!-- Hamburger -->
        <div class="flex items-center -mr-2 sm:hidden">
            <button x-on:click="open = ! open" class="inline-flex items-center justify-center p-2 text-white transition rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500">
                <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-jet-responsive-nav-link href="{{ route('dashboard') }}" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-jet-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <div class="flex-shrink-0 mr-3">
                        <img class="object-cover w-10 h-10 rounded-full" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->username }}" />
                    </div>
                @endif

                <div>
                    <div class="text-base font-medium text-gray-800">{{ Auth::user()->username }}</div>
                    <div class="text-sm font-medium text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Account Management -->
                <x-jet-responsive-nav-link href="{{ route('profile.show') }}" :active="request()->routeIs('profile.show')">
                    {{ __('Profile') }}
                </x-jet-responsive-nav-link>

                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                    <x-jet-responsive-nav-link href="{{ route('api-tokens.index') }}" :active="request()->routeIs('api-tokens.index')">
                        {{ __('API Tokens') }}
                    </x-jet-responsive-nav-link>
                @endif

                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-jet-responsive-nav-link href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                    this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-jet-responsive-nav-link>
                </form>

            </div>
        </div>
    </div>
</nav>
