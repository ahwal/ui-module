@props(['tabItems', 'xModel', 'showSelectForMobile' => false])

<div x-data="{tabs: {!! $tabItems !!}}"
    {{ $attributes }}
>
    <!-- Dropdown menu on small screens -->
    @if ($showSelectForMobile)
    <div class="sm:hidden">
        <label for="current-tab" class="sr-only">Select a tab</label>
        <select x-model="{{ $xModel }}" id="current-tab" name="current-tab"
            class="block w-full py-1 pl-3 pr-10 text-base border-gray-300 rounded-md focus:outline-none focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50 sm:text-sm">
            <template x-for="(label, name) in tabs" hidden>
            <option x-bind:value="name" x-text="label"></option>
            </template>
        </select>
    </div>
    @endif

    <!-- Tabs at small breakpoint and up -->
    <div
        @if ($showSelectForMobile)
        class="hidden sm:block"
        @endif
    >
        <nav class="flex -mb-px space-x-8">
            <template x-for="(label, name) in tabs" hidden>
            <button
                class="px-1 pb-4 text-sm font-medium border-b-2 border-transparent whitespace-nowrap"
                x-bind:class="{
                    'border-green-500 text-green-600': {{ $xModel }} == name,
                    'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300': {{ $xModel }} != name,
                }"
                x-text="label"
                x-on:click="{{ $xModel }} = name"
            ></button>
            </template>
        </nav>
    </div>
</div>
