@props(['active'])

@php
$classes = ($active ?? false)
            ? 'inline-flex items-center px-1 pt-1 border-b-4 border-green-600 text-[13px] leading-5 text-white focus:outline-none focus:border-green-400 transition'
            : 'inline-flex items-center px-1 pt-1 border-b-4 border-transparent text-[13px] leading-5 text-white hover:border-green-400 focus:outline-none focus:border-green-400 transition';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
