<x-ui::button
    {{ $attributes->merge([
        'class' => 'hover:bg-gray-100 bg-white border-gray-300'
    ]) }}
>
    {{ $slot }}
</x-ui::button>
