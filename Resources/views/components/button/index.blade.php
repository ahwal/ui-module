<button
    {{ $attributes->merge([
        'type' => 'button',
        'class' => 'py-1 px-3 border rounded text-[13px] text-gray-700 shadow active:shadow-0 leading-5 font-medium focus:outline-none focus:border-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50 transition duration-150 ease-in-out' .
            ' disabled:opacity-50 disabled:cursor-not-allowed',
    ]) }}
>
    {{ $slot }}
</button>
