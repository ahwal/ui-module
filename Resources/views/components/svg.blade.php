@props(['icon'])

@php
$path = module_path('UI', "Resources/icons/{$icon}.svg");

$svg = 'svg not found';

if (file_exists($path)) {
    $svg = file_get_contents($path);
    $svg = str_replace('<svg ', "<svg {$attributes} ", $svg);
}

echo $svg;
@endphp
