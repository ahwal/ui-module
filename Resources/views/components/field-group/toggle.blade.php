@props([
    'xModel',
    'notes' => null,
    'error' => 'false',
])

<x-ui::input.toggle
    x-model="{{ $xModel }}"
    {{ $attributes }}
/>

@if ($notes)
    <x-ui::input.notes>{{ $notes }}</x-ui::input.notes>
@endif
<x-ui::input.error>{!! $error !!}</x-ui::input.error>
