@props([
    'xModel',
    'options' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'orientation' => 'vertical',
    'notes' => null,
    'error' => 'false',
])

<x-ui::input.check
    options="{{ json_encode($options) }}"
    x-model="{{ $xModel }}"
    orientation="{{ $orientation }}"
    bind-class="{ '!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50': {!! $error !!} }"
    {{ $attributes }}
/>

@if ($notes)
    <x-ui::input.notes>{{ $notes }}</x-ui::input.notes>
@endif
<x-ui::input.error>{!! $error !!}</x-ui::input.error>
