@props([
    'xModel',
    'options' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'orientation' => null, {{-- vertical or horizontal --}}
    'notes' => null,
    'error' => 'false',
])

@php
    // if options count > 2, default to vertical
    $orientation = $orientation ?? (count($options) < 3 ? 'horizontal' : 'vertical');
@endphp

<x-ui::input.radio
    options="{{ json_encode($options) }}"
    x-model="{{ $xModel }}"
    orientation="{{ $orientation }}"
    bind-class="{ '!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50': {!! $error !!} }"
    {{ $attributes }}
/>

@if ($notes)
    <x-ui::input.notes>{{ $notes }}</x-ui::input.notes>
@endif
<x-ui::input.error>{!! $error !!}</x-ui::input.error>
