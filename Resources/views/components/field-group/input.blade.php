@props(['error' => 'false', 'notes' => null])

<x-ui::input
    {{ $attributes }}
    x-bind:class="{'!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50 ': {!! $error !!}}"
/>
@if ($notes)
    <x-ui::input.notes>{{ $notes }}</x-ui::input.notes>
@endif
<x-ui::input.error>{!! $error !!}</x-ui::input.error>
