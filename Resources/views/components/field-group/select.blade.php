@props([
    'xModel',
    'options' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'placeholder' => '-- Pilih --',
    'notes' => null,
    'error' => 'false',
])

<x-ui::input.select
    options="{{ json_encode($options) }}"
    x-model="{{ $xModel }}"
    bind-class="{ '!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50': {!! $error !!} }"
/>

@if ($notes)
    <x-ui::input.notes>{{ $notes }}</x-ui::input.notes>
@endif
<x-ui::input.error>{!! $error !!}</x-ui::input.error>
