@props(['label', 'labelClass' => ''])

<div {{ $attributes->merge([
        'class' => "text-sm sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
    ]) }} >
    <div class="font-medium text-gray-700 sm:mt-px sm:pt-1 {{ $labelClass }}">
        {{ $label }}
    </div>
    <div class="mt-1 sm:mt-0 sm:col-span-2">
        {{ $slot }}
    </div>
</div>
