{{--
@TODO: different notification type icon. Currently its all same success icon
--}}
<div
    x-data="notification"
    @notify.window="add($event.detail)"
    class="fixed inset-0 z-50 flex flex-col items-end justify-start px-4 py-4 space-y-4 pointer-events-none"
>
    <template x-for="notif of notifs" :key="notif.id">
        <div
            x-show="visible.includes(notif)"

            x-transition:enter="transform ease-out duration-300 transition"
            x-transition:enter-start="translate-x-full opacity-0"
            x-transition:enter-end="translate-x-0 opacity-100"

            x-transition:leave="transition ease-out duration-500"
            x-transition:leave-start="transform translate-y-0 opacity-100"
            x-transition:leave-end="transform -translate-y-full opacity-0"

            class="w-full max-w-sm bg-white rounded-lg shadow-lg pointer-events-auto"
        >
            <div class="overflow-hidden rounded-lg shadow-xs">
                <div class="p-4">
                    <div class="flex items-start">
                        <div class="flex-shrink-0">
                            <svg class="w-6 h-6 text-green-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                        </div>
                        <div class="ml-3 w-0 flex-1 pt-0.5">
                            <p x-text="notif.message" class="text-sm font-medium leading-5 text-gray-900"></p>
                        </div>
                        <div class="flex flex-shrink-0 ml-4">
                            <button @click="remove(notif.id)" class="inline-flex text-gray-400 transition duration-150 ease-in-out focus:outline-none focus:text-gray-500">
                                <svg class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>

<script>
window.notify = function (message, type, duration) {
    window.dispatchEvent(
        new CustomEvent('notify', {
            detail: {
                message,
                type: type || 'info',
                duration: duration || 4000,
            },
        })
    );
};

document.addEventListener('alpine:init', () => {
    Alpine.data('notification', () => ({
        notifs: [],
        visible: [],
        increment: 0, // used to prevent duplicate notifications
        add(notif) {
            notif.id = ++this.increment
            this.notifs.push(notif)
            // enter animation not works if not using nexttick
            this.$nextTick(() => this.fire(notif.id, notif.duration));
        },
        fire(id, duration) {
            this.visible.push(this.notifs.find(notif => notif.id == id))
            const time = (duration ?? 4000) * this.visible.length
            setTimeout(() => {
                this.remove(id)
            }, time)
        },
        remove(id) {
            const visibleNotif = this.visible.find(notif => notif.id == id)
            const index = this.visible.indexOf(visibleNotif)
            this.visible.splice(index, 1)
        },
    }));
});
</script>
