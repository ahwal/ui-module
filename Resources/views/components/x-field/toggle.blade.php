@props(['xModel', 'dataVar'])

<div
    x-data="{ on: false }"
    x-effect="$dispatch('change', on)"
    x-modelable="on"
    x-model="{{ $xModel }}"
    x-id="['x-field-toggle']"
    {!! $attributes !!}
>
    <div class="flex items-center">
        <button type="button"
            class="relative inline-flex flex-shrink-0 w-8 h-4 transition-colors duration-200 ease-in-out bg-green-600 border-2 border-transparent rounded-full cursor-pointer focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
            role="switch"
            x-ref="switch"
            x-state:on="Enabled"
            x-state:off="Not Enabled"
            x-bind:class="{ 'bg-green-600': on, 'bg-gray-200': !(on) }"
            x-bind:aria-labelledby="$id('x-field-toggle')"
            x-bind:aria-checked="on ? 'true' : 'false'"
            x-on:click="on = !on"
        >
            <span aria-hidden="true" class="inline-block w-3 h-3 transition duration-200 ease-in-out transform translate-x-4 bg-white rounded-full shadow pointer-events-none ring-0"
                x-state:on="Enabled"
                x-state:off="Not Enabled"
                x-bind:class="{ 'translate-x-4': on, 'translate-x-0': !(on) }"
            ></span>
        </button>
        <span
            class="ml-2 cursor-pointer"
            x-bind:id="$id('x-field-toggle')"
            x-on:click="on = !on; $refs.switch.focus()"
            x-text="{{ $dataVar }}.description"
        ></span>
    </div>

    <p
        x-show="{{ $dataVar }}.error"
        x-text="{{ $dataVar }}.error ?? ''"
    class="mt-1 text-xs text-red-500"></p>
</div>
