@props(['xModel', 'dataVar', 'type' => 'text', 'class' => ''])

<div>
    <input
        x-model="{{ $xModel }}"
        type="{{ $type }}"
        class="py-1 text-sm border-gray-300 rounded-md shadow-sm focus:border-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50 {{ $class }}"
        x-bind:class="{'!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50 ': {{ $dataVar }}.error}"
    />

    <p class="mt-1 text-xs text-gray-500"
        x-show="{{ $dataVar }}.description"
        x-text="{{ $dataVar }}.description ?? ''"
    ></p>

    <p
        x-show="{{ $dataVar }}.error"
        x-text="{{ $dataVar }}.error ?? ''"
    class="mt-1 text-xs text-red-500"></p>
</div>
