@props(['xModel', 'dataVar'])

<div
    class="mt-2"
    x-bind:class="{
        'flex space-x-4': {{ $dataVar }}.orientation == 'horizontal',
        'space-y-2': {{ $dataVar }}.orientation != 'horizontal'
    }"
    {!! $attributes !!}
>
    <select
        x-model="{{ $xModel }}"
        class="block py-1 pl-3 pr-10 text-base border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50 sm:text-sm"
        x-bind:class="{'!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50': {{ $dataVar }}.error}"
    >
        <option value="" x-text="{{ $dataVar }}.placeholder ?? 'Select'"></option>
        <template x-for="key in Object.keys({{ $dataVar }}.options)" :key="key" hidden>
            <option x-bind:value="key" x-text="{{ $dataVar }}.options[key]"></option>
        </template>
    </select>

    <p class="mt-1 text-xs text-gray-500"
        x-show="{{ $dataVar }}.description"
        x-text="{{ $dataVar }}.description ?? ''"
    ></p>

    <p
        x-show="{{ $dataVar }}.error"
        x-text="{{ $dataVar }}.error ?? ''"
    class="mt-1 text-xs text-red-500"></p>
</div>
