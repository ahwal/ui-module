@props(['dataVar', 'dataModel'])

<template x-if="{{ $dataVar }}.type == 'text'">
    <x-ui::x-field.input
        class="w-full max-w-lg sm:max-w-xs"
        x-model="{{ $dataModel }}"
        data-var="{{ $dataVar }}"
    />
</template>
<template x-if="{{ $dataVar }}.type == 'check'">
    <x-ui::x-field.check
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    />
</template>
<template x-if="{{ $dataVar }}.type == 'option'">
    <x-ui::x-field.radio
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    />
</template>
<template x-if="{{ $dataVar }}.type == 'select'">
    <x-ui::x-field.select
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    ></x-ui::x-field.select>
</template>
<template x-if="{{ $dataVar }}.type == 'select-data'">
    <x-ui::x-field.select-data
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    ></x-ui::x-field.select-data>
</template>
<template x-if="{{ $dataVar }}.type == 'select-finder'">
    <x-ui::x-field.select-finder
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    ></x-ui::x-field.select-finder>
</template>
<template x-if="{{ $dataVar }}.type == 'toggle'">
    <x-ui::x-field.toggle
        data-var="{{ $dataVar }}"
        x-model="{{ $dataModel }}"
    ></x-ui::x-field.toggle>
</template>
