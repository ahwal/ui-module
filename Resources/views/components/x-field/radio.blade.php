@props(['xModel', 'dataVar'])

<div
    class="mt-2"
    x-bind:class="{
        'flex space-x-4': {{ $dataVar }}.orientation == 'horizontal',
        'space-y-2': {{ $dataVar }}.orientation != 'horizontal'
    }"
    {!! $attributes !!}>
    <template x-for="key in Object.keys({{ $dataVar }}.options)" :key="key" hidden>
        <div class="relative flex items-start" x-id="['x-field-radio']">
            <div class="flex items-center h-5">
                <input
                    type="radio"
                    class="w-4 h-4 text-green-600 border-gray-300 rounded-full focus:ring-green-500 focus:ring-opacity-50"
                    x-model="{{ $xModel }}"
                    x-bind:id="$id('x-field-radio', key)"
                    x-bind:name="key"
                    x-bind:value="key"
                    x-bind:class="{'!border-red-500 focus:!ring-red-500 focus:!ring-opacity-50': {{ $dataVar }}.error}"
                >
            </div>
            <div class="ml-2 text-sm">
                <label x-bind:for="$id('x-field-radio', key)"
                    class="font-medium text-gray-700 cursor-pointer"
                    x-text="{{ $dataVar }}.options[key]"
                ></label>
            </div>
        </div>
    </template>

    <p class="mt-1 text-xs text-gray-500"
        x-show="{{ $dataVar }}.description"
        x-text="{{ $dataVar }}.description ?? ''"
    ></p>

    <p
        x-show="{{ $dataVar }}.error"
        x-text="{{ $dataVar }}.error ?? ''"
    class="mt-1 text-xs text-red-500"></p>
</div>
