@props([
    'xModel',

    'dataHub',
    'dataColumns' => 'id,name',
    'dataFor' => '',

    'value' => null,
    'label' => null,

    'searchLabel' => 'Cari data...',
    'trigger' => null,
])

{{-- Use wrapper div to allow passing event, like x-on:change --}}
<div
    x-data="selectData"

    value="{{ $value }}"
    label="{{ $label }}"

    data-hub="{{ $dataHub }}"
    data-columns="{{ $dataColumns }}"
    data-for="{{ $dataFor }}"

    {{ $attributes->merge(['class' => 'relative text-sm']) }}
>
    <div
        x-ref="root"
        x-on:keydown.escape.window="clearSearch()"
        @if ($xModel) x-on:change="{{ $xModel }} = $event.detail" @endif

        x-init="async () => {
            @if ($xModel)
            _data.initialModelValue = {{ $xModel }}

            $watch('{{ $xModel }}', val => {
                if (typeof val == 'undefined' || val === null) {
                    _data.value = $el.parentElement.getAttribute('value') == '' ? null : $el.parentElement.getAttribute('value')
                    _data.label = $el.parentElement.getAttribute('label') == '' ? null : $el.parentElement.getAttribute('label')

                    return
                }

                if (!_data.data.length) return

                const found = _data.data.find(item => item.id == {{ $xModel }})
                if (found) {
                    _data.value = found.id
                    _data.label = found.name
                }
            })
            @endif
        }"
    >
        @if ($trigger)
            <div
                x-on:click="_data.open = !_data.open; if (_data.open) onOpen($event)"
                x-ref="trigger"
            >
                {{ $trigger }}
            </div>
        @else
        <button
            type="button"
            x-on:click="_data.open = !_data.open; if (_data.open) onOpen($event)"
            class="relative w-full py-1 pl-2 pr-5 text-left truncate border border-gray-300 rounded-md shadow-sm trigger focus:outline-none focus:border-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50"
            x-bind:class="{ 'text-gray-500': !_data.value }"
            x-ref="trigger"
        >
            <span x-text="_data.label ?? 'Pilih Data'"></span>
            <x-ui::svg icon="chevron-down"
                class="absolute top-[calc(50%-(1.25rem/2))] right-[calc(1.25rem/4)] w-5 h-5 duration-300 transform"
                x-bind:class="{ 'rotate-180': _data.viewMode == 'filter' }" />
        </button>
        @endif

        <div x-show="_data.open" class="fixed inset-0 z-10 transition-all transform"
            x-on:click="close(true)"
            x-on:wheel="close(true)"
            x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
            x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
        >
        </div>

        <div x-show="_data.open" x-transition:enter="transition ease-out duration-200"
            x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100"
            x-transition:leave-end="transform opacity-0 scale-95"
            class="absolute z-50 w-full mt-1 origin-top rounded-md shadow-lg" style="display: none;">

            <div class="relative w-full bg-white rounded-md ring-1 ring-black ring-opacity-5">

                <x-ui::input type="text"
                    class="block w-full px-2 py-1 text-sm"
                    placeholder="{{ $searchLabel }}"
                    x-bind="binds.searchInput"
                    x-model="_data.search"
                    x-ref="inputSearch"
                ></x-ui::input>
                <div
                    x-bind="binds.searchResults"
                    x-ref="results"
                    class="z-50 w-full py-1 mt-1 overflow-y-scroll origin-top bg-white rounded-md shadow-lg max-h-[18.7rem]"
                >
                    <template x-for="(item, index) in _data.filtered" :key="item.id">
                        <div
                            class="text-sm cursor-pointer"
                            x-on:click="select()"
                            x-bind:class="{'bg-green-100 bg-opacity-50': _data.selected == index}"
                            x-on:mouseover="_data.selected = index"
                        >
                            {{ $slot }}
                        </div>
                    </template>
                </div>
            </div>

        </div>
    </div>
</div>

@once
<script>
function selectData() {
    return {
        _data: {
            open: false,
            data: [],
            filtered: [],
            selected: 0,
            value: null,
            label: null,
            search: '',
            initialModelValue: null,
            filter: {},
        },

        dataHub: null,
        dataColumns: null,

        async init() {
            // load data from DOM
            this.dataHub = this.$el.getAttribute('data-hub')
            this.dataColumns = this.$el.getAttribute('data-columns')
            this.dataFor = this.$el.getAttribute('data-for') // used by getChoices

            this._data.value = this.$el.getAttribute('value') == '' ? null : this.$el.getAttribute('value')
            this._data.label = this.$el.getAttribute('label') == '' ? null : this.$el.getAttribute('label')

            if (this.$el.dataset && this.$el.dataset.filter) {
                const filter = window.parseStringToObject(this.$el.dataset.filter)
                _.merge(this._data.filter, filter)
            }

            this.$watch('_data.search', val => {
                this._data.selected = 0
                this._data.filtered = this.doFilter(val)
            })

            await this.loadData()

            if (!this._data.initialModelValue) return;

            const found = this._data.data.find(item => item.id == this._data.initialModelValue)
            if (found) {
                this.$nextTick(() => {
                    this._data.value = found.id
                    this._data.label = found.name
                })
            }
        },

        binds: {
            backCover: {
                ['x-show']() {
                    return this._data.open
                },
                ['@click']() {
                    this.close(true)
                },
                ['@wheel']() {
                    this.close(true)
                },
            },
            searchWrap: {
                ['x-show']() {
                    return this._data.open
                },
                ['x-transition:enter']() {
                    return "transition ease-out duration-200"
                },
                ['x-transition:enter-start']() {
                    return "transform opacity-0 scale-95"
                },
                ['x-transition:enter-end']() {
                    return "transform opacity-100 scale-100"
                },
                ['x-transition:leave']() {
                    return "transition ease-in duration-75"
                },
                ['x-transition:leave-start']() {
                    return "transform opacity-100 scale-100"
                },
                ['x-transition:leave-end']() {
                    return "transform opacity-0 scale-95"
                },
            },
            searchInput: {
                ['@keydown.escape']() {
                    this.clearSearch()
                },
                ['@keydown.tab']() {
                    this.close()
                },
                ['@change.stop']() {},
                ['@keydown.enter.prevent']() {
                    // Trigger click on selected item list.
                    // Directly call this.select() from here not work,
                    // because the 'change' event will be dispatched from
                    // this input element and stopped
                    this.$refs.results.querySelectorAll('.cursor-pointer')[this._data.selected].click()
                },
                ['@keydown.arrow-down.prevent']() {
                    this.keyDown()
                },
                ['@keydown.arrow-up.prevent']() {
                    this.keyUp()
                },
            },
            searchResults: {
                ['x-transition:enter']() {
                    return "transition ease-out duration-200"
                },
                ['x-transition:enter-start']() {
                    return "transform opacity-0 scale-95"
                },
                ['x-transition:enter-end']() {
                    return "transform opacity-100 scale-100"
                },
                ['x-transition:leave']() {
                    return "transition ease-in duration-75"
                },
                ['x-transition:leave-start']() {
                    return "transform opacity-100 scale-100"
                },
                ['x-transition:leave-end']() {
                    return "transform opacity-0 scale-95"
                },
            },

        },

        doFilter(search) {
            if (search == '' || !search) return this._data.data // .slice(0, 5)
            return this._data.data.filter(item => item.name.match(new RegExp(search,"gi")))
        },

        applyFilters(filter, replace) {
            if (replace) {
                this._data.filter = _.clone(filter)
            } else {
                _.merge(this._data.filter, filter)
            }
            this.loadData()
        },

        async loadData() {
            let params = {columns: this.dataColumns}

            if (this.dataFor) params['for'] = this.dataFor
            if (Object.keys(this._data.filter).length) params['filter'] = this._data.filter

            this._data.data = await dataHub(this.dataHub, params)
        },

        async onOpen($event) {
            if (!this._data.data.length) {
                await this.loadData()
            }

            this._data.filtered = this.doFilter(this._data.search)

            const input = this.$refs.root.querySelector('input')
            this.$nextTick(() => {
                if ($event.key) input.value = $event.key
                else input.select()

                input.focus()
            })
        },

        clearSearch() {
            if (this._data.search == '') this.close()

            this._data.search = ''
            this.$nextTick(() => this.$refs.inputSearch.focus())
        },

        async close() {
            this._data.open = false
            this.$nextTick(() => this.$refs.trigger.focus())
        },

        select() {
            const val = this._data.filtered[this._data.selected]

            if (typeof val == 'undefined') return

            this._data.value = val.id
            this._data.label = val.name

            this.$dispatch('change', this._data.value)
            this.close()
        },

        keyDown() {
            this._data.selected++
            if (this._data.selected >= this._data.filtered.length) this._data.selected = 0
        },
        keyUp() {
            this._data.selected--
            if (this._data.selected < 0) this._data.selected = this._data.filtered.length - 1
        },
    }
}
</script>
@endonce
