{{-- TODO: simplify by merging with select-list.index --}}

@props([
    'xModel',
    'dataHub',
    'dataKey' => 'id',
    'height' => '300px',
    'dataRelations' => '',
    'dataClass' => null
])

<div x-data="genericSelectList"
    x-init="() => {
        {{-- JS code is pushed once, so we need to do this for dynamic $xModel --}}
        $watch('{{ $xModel }}', val => modelWatched(val))

        {{-- ================================== --}}
        {{-- Inject function to x-data, so we can use PHP variable $xModel --}}
        {{-- ================================== --}}
        $el._x_dataStack[0].setModel = value => {{ $xModel }} = value;
        $el._x_dataStack[0].getModel = () => {{ $xModel }}
    }"

    data-key="{{ $dataKey }}"
    data-hub="{{ $dataHub }}"
    data-relations="{{ $dataRelations }}"

    tabindex="0"

    {{ $attributes }}
>
    <div class="grid grid-rows-[1fr,30px] border-t relative" style="height: {{ $height }}">
        {{-- ============================= --}}
        {{--   refresh loading indicator   --}}
        {{-- ============================= --}}
        <div class="absolute grid w-full bg-white place-content-center top-0 bottom-[30px] border-l border-r"
            x-show="_data.isLoading && !_data.data.length"
        >
            <x-ui::svg icon="refresh"
                class="w-6 h-6 text-gray-400 hover:text-gray-700"
                x-bind:class="{'animate-spin': _data.isLoading}" />
        </div>

        <x-ui::list.virtual
            x-list="_data.data"
            x-for="item in list.visibleData"
            x-bind:key="item.index"
            x-on:scroll-end="getNextData"
            class="border-l border-r select-none"
            list-class="divide-y divide-solid"
        >
            <x-slot name="footer">
                <div x-show="_data.hasMoreData" class="grid h-20 place-content-center">Loading...</div>
            </x-slot>

            <div
                @if ($dataClass)
                    class="{{ $dataClass }}"
                @else
                    class="p-2 flex justify-between text-[13px] hover:bg-gray-100 cursor-pointer group"
                @endif
                x-bind:class="{'!bg-blue-100': isSelected(item.data.{{ $dataKey }})}"
                x-on:click="clickOnListItem(item)"
            >
                @if ($slot == '')
                    <div class="w-1/2 px-2">
                        <div x-text="item.data.name"></div>
                    </div>
                    <div class="w-[20%]"></div>
                @else
                    {{ $slot }}
                @endif
            </div>
        </x-ui::list.virtual>


        {{-- ========================= --}}
        {{--   footer   --}}
        {{-- ========================= --}}
        <div class="flex items-center px-2 space-x-2 text-xs border">
            <a href="#" x-on:click.prevent="refreshData">
                <x-ui::svg icon="refresh"
                    class="w-4 h-4 text-gray-400 hover:text-gray-700"
                    x-bind:class="{'animate-spin': _data.isLoading}" />
            </a>
            <span x-text="_data.totalData"></span>
        </div>
    </div>
</div>


@once
<script>
function genericSelectList() {
    return {
        // prefix data to prevent confusion with other alpine components
        _data: {
            filter: {},
            data: [],
            nextData: [],
            totalData: 0,
            totalPage: 0,
            page: 0,
            isLoading: false,
            hasMoreData: true,
            selectedRowIndex: null,
            showFilter: false,
        },

        dataHub: null,
        dataKey: null,
        dataRelations: null,

        async init() {
            this.dataHub = this.$el.getAttribute('data-hub')
            this.dataKey = this.$el.getAttribute('data-key')
            this.dataRelations = this.$el.getAttribute('data-relations')

            // use alpine dataset, getting data from data-filter attribute
            if (this.$el.dataset && this.$el.dataset.filter) {
                const filter = window.parseStringToObject(this.$el.dataset.filter)
                _.merge(this._data.filter, filter)
            }

            await this.getNextData()
        },

        modelWatched(val) {
            if (val == 'new') {
                this._data.selectedRowIndex = null;
                return;
            }

            let found = false

            // find data and row index
            this._data.data.forEach((data, idx) => {
                if (data[this.dataKey] == val) {
                    found = data
                    this._data.selectedRowIndex = idx
                }
            })

            if (found) {
                this.$dispatch('change', _.clone(found))
            } else {
                this.clearSelection(true) // force
            }
        },

        applyFilters(filter, replace) {
            if (replace) {
                this._data.filter = _.clone(filter)
            } else {
                _.merge(this._data.filter, filter)
                // trigger $watch on filter
                this._data.filter = _.clone(this._data.filter)
            }

            return this
        },

        clearSelection(force) {
            if (this._data.selectedRowIndex === null && !force) return;

            this.setModel(null);

            this._data.selectedRowIndex = null;

            this.$dispatch('change', null)
        },

        isSelected(dataId) {
            return this.getModel() == dataId
        },

        clickOnListItem(item) {
            if (this.isSelected(item.data[this.dataKey])) {
                this.clearSelection()
            } else {
                this._data.selectedRowIndex = item.index;
                this.setModel(item.data[this.dataKey]);
                {{-- this.$dispatch('change',  _.clone(item.data)) --}}
            }
        },

        async refreshData(selectedData) {
            this._data.data = this._data.nextData = []
            this._data.page = 0
            this._data.hasMoreData = true

            if (!selectedData) {
                this.clearSelection()
            }

            await this.getNextData()

            if (selectedData && selectedData[this.dataKey]) {
                this.setModel(selectedData[this.dataKey])
            }
        },

        async getNextData() {
            if (this._data.isLoading || !this._data.hasMoreData) return

            // concat data loaded previously
            if (this._data.data.length && this._data.nextData.length) {
                this._data.data = this._data.data.concat(this._data.nextData)
                this._data.nextData = []
                this._data.page++
                this.$dispatch('data-loaded');
            }

            if (this._data.data.length > 1 && this._data.data.length >= this._data.totalData) {
                this._data.hasMoreData = false
                return;
            }

            this._data.isLoading = true

            let params = {page: this._data.page + 1}
            if (Object.keys(this._data.filter).length)
                params['filter'] = this._data.filter
            if (this.dataRelations)
                params['relations'] = this.dataRelations

            // preload next data
            const response = await dataHub(this.dataHub, params)

            this._data.isLoading = false

            // first page
            if (!this._data.data.length) {
                this._data.data = response.data
                this._data.page = 1
            } else {
                this._data.nextData = response.data
            }

            if (this._data.page >= response.last_page) {
                this._data.hasMoreData = false
            }

            this._data.totalData = response.total

            if (this._data.page == 1 && response.last_page > 1) this.getNextData()

            this.$dispatch('data-loaded');
        },
        isRowSelected(index) {
            return this._data.selectedRowIndex == index
        },

        updateData(id, newData) {
            console.log(id, newData)
            this._data.data.forEach(data => {
                if (data.id == id) {
                    Object.keys(newData).forEach(key => data[key] = newData[key])
                }
            })
        },
    }
}
</script>
@endonce
