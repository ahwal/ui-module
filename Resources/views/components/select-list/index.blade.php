@props([
    'xModel',
    'dataHub',
    'dataRelations' => '',
    'dataKey' => 'id',
    'height' => '300px',

    'withSearch' => false,
    'withFilter' => false,
    'withOrder' => false,

    // slots
    'listing',
    'filter' => null,
    'order' => null,
])

@php
    $hasTopBar = $withSearch || $withFilter || $withOrder;
    $randomStringID = Illuminate\Support\Str::random(5);
@endphp

<div x-data="selectList"
    x-ref="root"
    x-modelable="_data.selected"
    x-model="{{ $xModel }}"
    x-effect="modelWatched(_data.selected)"

    data-relations="{{ $dataRelations }}"
    data-hub="{{ $dataHub }}"
    data-key="{{ $dataKey }}"
    data-id="{{ $randomStringID }}"

    tabindex="0"

    {{ $attributes }}
>
    <div
        x-bind:class="{
            '!grid-rows-[60px,1fr,30px]': hasFilter()
        }"
        class="grid
            @if($hasTopBar)
                grid-rows-[30px,1fr,30px]
            @else
                grid-rows-[1fr,30px] border-t
            @endif
            relative border-l border-r"
        style="height: {{ $height }}"
        x-on:change.stop {{-- prevent unwanted "change" event to propagating to the parent --}}
    >

        {{-- ============================= --}}
        {{--   refresh loading indicator   --}}
        {{-- ============================= --}}
        <div class="absolute grid w-full bg-white place-content-center top-[30px] bottom-[30px] border-l border-r"
            x-show="_data.isLoading && _data.data.length == 0"
        >
            <x-ui::svg icon="refresh"
                class="w-6 h-6 text-gray-400 hover:text-gray-700"
                x-bind:class="{'animate-spin': _data.isLoading}" />
        </div>


        {{-- ============================= --}}
        {{--   search, filter, order bar   --}}
        {{-- ============================= --}}
        @if($hasTopBar)
        <div class="-mx-px overflow-hidden js-filter-bar">
            <div class="flex h-[30px]">

                @if($withSearch)
                <div class="relative flex items-stretch flex-grow focus-within:z-10">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-1 text-gray-500 pointer-events-none">
                        <x-ui::svg icon="search" class="w-4 h-4" />
                    </div>
                    <input
                        x-on:keydown="_data.viewMode = 'list'"
                        x-on:keydown.escape="delete _data.filter.search"
                        x-model.debounce="_data.filter.search"
                        type="text" class="block w-full pl-6 text-sm border-gray-200 focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50"
                        placeholder="Pencarian..."
                    />
                    <x-ui::svg icon="x"
                        x-show="_data.filter.search"
                        x-on:click="delete _data.filter.search"
                        class="absolute w-4 h-4 text-gray-300 cursor-pointer right-1 top-2 hover:text-gray-700"
                    />
                </div>
                @endif

                @if($withOrder)
                <button
                    type="button"
                    class="inline-flex items-center px-2 py-2 space-x-1 text-sm font-medium text-gray-700 border bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50"
                    x-on:click="_data.viewMode = _data.viewMode == 'order' ? 'list' : 'order'"
                >
                    <x-ui::svg icon="sort"
                        class="inline-block w-5 h-5"
                    />
                </button>
                @endif

                @if($withFilter)
                <button
                    x-on:click="_data.viewMode = _data.viewMode == 'filter' ? 'list' : 'filter'"
                    x-bind:class="{'!bg-gray-300': _data.viewMode == 'filter'}"
                    type="button"
                    class="relative inline-flex items-center px-2 py-2 space-x-1 text-sm font-medium text-gray-700 border bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500"
                >
                    <span>Filter</span>
                    <x-ui::svg icon="chevron-down"
                        class="inline-block w-5 h-5 duration-300 transform"
                        x-bind:class="{ 'rotate-180': _data.viewMode == 'filter' }" />
                </button>
                @endif
            </div>

            <div
                class="h-[30px] overflow-x-auto border-b whitespace-nowrap text-sm flex items-center px-2 space-x-1 scrollbar-hide"
                x-show="hasFilter()"
            >
                <template x-for="filter in Object.keys(_data.filter)">
                    <div class="flex items-center">
                        <span x-text="filter + '=' + _data.filter[filter]"></span>
                        <x-ui::svg icon="x"
                            class="w-4 h-4 text-gray-300 cursor-pointer hover:text-gray-600"
                            x-on:click="() => {
                                delete _data.filter[filter];
                                _data.filter = _.clone(_data.filter)
                                $dispatch('filterdelete', filter)
                            }
                            "
                        />
                    </div>
                </template>
            </div>
        </div>
        @endif {{-- $hasTopBar --}}


        {{-- =============================== --}}
        {{--   filter & order close button   --}}
        {{-- =============================== --}}
        {{-- <x-ui::svg icon="x" class="absolute w-6 h-6 text-gray-400 cursor-pointer right-1 top-9 hover:text-gray-700"
            x-show="_data.viewMode != 'list'"
            x-on:click="_data.viewMode = 'list'"
        /> --}}


        {{-- ========================= --}}
        {{--      filter detail        --}}
        {{-- ========================= --}}
        @if($withFilter)
        <div
            x-show="_data.viewMode == 'filter'"
            x-data="{filter: {}}"
            x-init="$watch('_data.viewMode', val => {
                if (val == 'filter') filter = _.clone(_data.filter)
            })"
            class="p-2 space-y-3 text-sm border-l border-r"
        >
            {{ $filter }}

            <div class="flex">
                <x-ui::button.primary
                    x-on:click="_data.filter = _.clone(filter); _data.viewMode = 'list';"
                >Terapkan</x-ui::button.primary>
                <x-ui::button.secondary
                    x-on:click="_data.filter = {}; _data.viewMode = 'list'"
                    class="ml-auto"
                >Reset Filter</x-ui::button.secondary>
            </div>

        </div>
        @endif


        {{-- ======================== --}}
        {{--      order detail        --}}
        {{-- ======================== --}}
        @if($withOrder)
        <div x-show="_data.viewMode == 'order'" class="p-2 text-sm border-l border-r">
            {{ $order }}
        </div>
        @endif



        {{-- ========================= --}}
        {{--   data list   --}}
        {{-- ========================= --}}
        <x-ui::list.virtual
            data-id="list-{{ $randomStringID }}" {{-- Used for reference, it's error when using default id attribute --}}
            x-ref="list"
            x-show="_data.viewMode == 'list'"
            x-list="_data.data"
            x-for="item in list.visibleData"
            x-bind:key="item.index"
            x-on:scroll-end="getNextData"
            class="select-none"
            list-class="divide-y divide-solid"
            tabindex="0" {{-- allow element to get focus --}}
        >
            <x-slot name="footer">
                <div x-show="_data.hasMoreData" class="grid h-20 place-content-center" x-on:mousemove="$dispatch('data-redraw')">Loading...</div>
            </x-slot>

            <div x-on:click="clickOnListItem(item)">
                {{ $listing }}
            </div>
        </x-ui::list.virtual>

        {{-- ========================= --}}
        {{--   footer   --}}
        {{-- ========================= --}}
        <div class="flex items-center px-2 -mx-px space-x-2 text-xs border">
            <a href="#" x-on:click.prevent="refreshData">
                <x-ui::svg icon="refresh"
                    class="w-4 h-4 text-gray-400 hover:text-gray-700"
                    x-bind:class="{'animate-spin': _data.isLoading}" />
            </a>
            <span x-text="_data.totalData"></span>
        </div>
    </div>
</div>


@once
<script>
function selectList() {
    return {
        // prefix data to prevent confusion with other alpine components
        _data: {
            filter: {},
            data: [],
            nextData: [],
            totalData: 0,
            page: 0,
            isLoading: false,
            hasMoreData: true,
            selectedRowIndex: null,
            viewMode: 'list',
            showFilter: false,
            selected: null,
        },

        dataID: null,
        dataHub: null,
        dataKey: null,
        dataRelations: null,

        async init() {
            // {{-- paramRelations data is passed via node attribute "data-relations" --}}
            this.dataRelations = this.$el.getAttribute('data-relations')

            this.dataHub = this.$el.getAttribute('data-hub')
            this.dataKey = this.$el.getAttribute('data-key')
            this.dataID = this.$el.getAttribute('data-id')

            // use alpine dataset, getting data from data-filter attribute
            if (this.$el.dataset && this.$el.dataset.filter) {
                const filter = window.parseStringToObject(this.$el.dataset.filter)
                _.merge(this._data.filter, filter)
            }

            this.$watch('_data.filter', val => this.filterWatched(val))
            this.$watch('_data.filter.search', () => this.refreshData())

            await this.getNextData()
        },

        filterWatched(val) {
            this.$dispatch('filterchange', val)
            this.refreshData()
        },

        modelWatched(val) {
            if (val == 'new') {
                this._data.selectedRowIndex = null;
                return;
            }

            let found = false

            // find data and row index
            this._data.data.forEach((data, idx) => {
                if (data[this.dataKey] == val) {
                    found = data
                    this._data.selectedRowIndex = idx
                }
            })

            if (found) {
                this.$dispatch('change', _.cloneDeep(found))
            } else {
                this.clearSelection(true) // force
            }
        },

        applyFilters(filter, replace) {
            if (replace) {
                this._data.filter = _.clone(filter)
            } else {
                _.merge(this._data.filter, filter)
                // trigger $watch on filter
                this._data.filter = _.clone(this._data.filter)
            }

            return this
        },

        clearSelection(force) {
            if (this._data.selectedRowIndex === null && !force) return;

            this._data.selected = null;

            this._data.selectedRowIndex = null;

            this.$dispatch('change', null)
        },

        isSelected(data) {
            return this._data.selected == data[this.dataKey]
        },

        clickOnListItem(item) {

            if (this.isSelected(item.data)) {
                this.clearSelection()
            } else {
                this._data.selectedRowIndex = item.index;
                this._data.selected = item.data[this.dataKey];
                this.$dispatch('change',  _.cloneDeep(item.data))
            }
        },

        resetData() {
            this._data.data = []
            this._data.nextData = []
            this._data.page = 0
            this._data.hasMoreData = true

            this.redrawList()
        },

        redrawList() {
            xData('[data-id="list-' + this.dataID + '"]').resetScroll()
        },

        async refreshData(selectedData) {
            this.resetData()

            if (!selectedData) {
                this.clearSelection()
            }

            await this.getNextData()

            if (selectedData && selectedData[this.dataKey]) {
                this._data.selected = selectedData[this.dataKey];
            }
        },

        async getNextData() {
            if (this._data.isLoading || !this._data.hasMoreData) return

            // concat data loaded previously
            if (this._data.data.length && this._data.nextData.length) {
                this._data.data = this._data.data.concat(this._data.nextData)
                this._data.nextData = []
                this._data.page++
                xData('[data-id="list-' + this.dataID + '"]').calculate()
            }

            if (this._data.data.length > 1 && this._data.data.length >= this._data.totalData) {
                this._data.hasMoreData = false
                return;
            }

            this._data.isLoading = true

            let params = {page: this._data.page + 1}
            if (Object.keys(this._data.filter).length)
                params['filter'] = this._data.filter
            if (this.dataRelations)
                params['relations'] = this.dataRelations

            // preload next data
            const response = await dataHub(this.dataHub, params)

            this._data.isLoading = false

            // first page
            if (!this._data.data.length) {
                this._data.data = response.data
                this._data.page = 1
            } else {
                this._data.nextData = response.data
            }

            if (this._data.page >= response.last_page) {
                this._data.hasMoreData = false
            }

            this._data.totalData = response.total

            if (this._data.page == 1 && response.last_page > 1) this.getNextData()

            xData('[data-id="list-' + this.dataID + '"]').calculate()
        },

        async getData(id) {
            const found = this._data.data.find(f => f.id == id)
            if (found) return found

            const result = await dataHub(this.dataHub, {
                relations: this.dataRelations,
                filter: {ids: id}
            })

            return result.data && result.data.length ? result.data[0] : null
        },

        isRowSelected(index) {
            return this._data.selectedRowIndex == index
        },
        hasFilter() {
            if (Object.keys(this._data.filter).length == 1 && this._data.filter['search'])
                return false

            return Object.keys(this._data.filter).length > 0
        },
    }
}
</script>
@endonce
