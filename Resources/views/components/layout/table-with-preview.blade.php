@props([
    'title',
    'list',
    'detail',
    'detailTitle',
    'detailActions',
    'selectedVar',
    'afterTitle' => null,
    'actionsVisible' => '1'
])

<div class="h-screen pt-10 bg-white" x-cloak {{ $attributes }}>

    <div class="flex items-center p-4 space-x-2 h-[60px]">
        <x-ui::button x-show="{{ $selectedVar }}" class="block !px-1 !py-0 mr-2" x-on:click="{{ $selectedVar }} = null">
            <x-ui::svg icon="chevron-left" class="w-5 h-5" />
        </x-ui::button>
        <h1 class="text-lg font-medium leading-6 text-gray-900">
            <span x-show="!{{ $selectedVar }}">{{ $title }}</span>
            <span x-show="{{ $selectedVar }}">{{ $detailTitle }}</span>
        </h1>
        {{ $afterTitle ?? '' }}
    </div>

    <div class="md:grid md:grid-cols-[1fr,300px] gap-4 md:h-[calc(100vh-140px)] px-4">
        <div
            class="hidden md:block"
            x-bind:class="{ '!block': !{{ $selectedVar }} }"
        >
            {{ $list }}
        </div>
        <div
            class="hidden border shadow md:block"
            x-bind:class="{'!block': {{ $selectedVar }}}"
            x-transition:enter="transform transition ease-in-out duration-500 sm:duration-700"
            x-transition:enter-start="translate-x-full"
            x-transition:enter-end="translate-x-0"
        >
            <div x-show="!{{ $selectedVar }}" class="grid h-full place-content-center min-h-[10rem]">
                <p>Belum ada data yang dipilih</p>
            </div>

            <div
                class="grid grid-rows-[1fr,56px] h-[calc(100vh-140px)]"
                x-show="{{ $selectedVar }}"
            >
                <div class="overflow-y-auto">
                    <div class="p-4 bg-white sm:p-6">
                        <div class="-mx-4 sm:-mx-6">
                            {{ $detail }}
                        </div>
                    </div>
                </div>

                <div x-show="{{ $actionsVisible }}" class="flex items-center px-4 border-t sm:px-6 bg-gray-50">
                    {{ $detailActions }}
                </div>

            </div>
        </div>
    </div>
</div>
