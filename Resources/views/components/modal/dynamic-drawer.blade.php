<div x-data="dynamicDrawerComponent" x-bind="backdrop" x-show="open" class="fixed inset-0 z-40 overflow-hidden"
    aria-labelledby="drawer-title" aria-modal="true">
    <div class="absolute inset-0 overflow-hidden">

        <div x-show="open" x-on:click="open = false" x-transition:enter="ease-in-out duration-500"
            x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
            x-transition:leave="ease-in-out duration-500" x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0" class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75"
            aria-hidden="true"></div>

        <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">

            <div x-show="open" x-transition:enter="transform transition ease-in-out duration-500 sm:duration-700"
                x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0"
                x-transition:leave="transform transition ease-in-out duration-500 sm:duration-700"
                x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full"
                class="relative w-screen" x-bind:class="params.maxWidthClass">

                <div x-show="open" x-transition:enter="ease-in-out duration-500" x-transition:enter-start="opacity-0"
                    x-transition:enter-end="opacity-100" x-transition:leave="ease-in-out duration-500"
                    x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
                    class="absolute top-0 left-0 flex pt-4 pr-2 -ml-8 sm:-ml-10 sm:pr-4">
                    <button
                        class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                        x-on:click="open = false">
                        <span class="sr-only">Close panel</span>
                        <svg class="w-6 h-6" x-description="Heroicon name: outline/x" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </button>
                </div>

                <template x-if="open">
                    <div class="grid grid-rows-[60px,1fr] h-screen bg-white shadow-xl" x-init="prepareContent">
                        <div class="flex px-4 py-4 border-b sm:px-6">
                            <h2 class="text-lg font-medium text-gray-900" id="drawer-title"
                                x-text="params.title ?? 'Panel Title'"></h2>
                            <div class="flex ml-auto" ref="action" x-init="prepareActionButton"></div>
                        </div>
                        <div class="relative px-4 overflow-y-auto sm:px-6">
                            <!-- Replace with your content -->
                            <div class="absolute inset-0 px-4 sm:px-6" x-bind="contentArea" ref="content">
                                <div class="h-full border-2 border-gray-200 border-dashed" aria-hidden="true"></div>
                            </div>
                            <!-- /End replace -->
                        </div>
                    </div>
                </template>
            </div>

        </div>
    </div>
</div>

<script>
    window.openDrawer = function (selector, params) {
        if (typeof params == 'undefined' || typeof params.callback == 'undefined') {
            alert('Callback required by openDrawer');
        }

        window.dispatchEvent(
            new CustomEvent('open-drawer', {
                detail: { selector, ...params, },
            })
        );
    };

    function dynamicDrawerComponent() {
        return {
            open: false,
            params: {},
            data: {},
            init() {
                this.$watch('open', val => {
                    // if (!val && window.history.state == '#open') history.go(-1)
                })
            },
            async submit() {
                if (await this.params.callback(this.data)) this.open = false
            },
            unwrap(node) {
                return node.replaceWith(...node.childNodes);
            },
            prepareContent() {
                //history.pushState('#open', null, '#open')
                this.$nextTick(() => {
                    if (!document.querySelector(this.params.selector)) {
                        console.log('Element for modal not found')
                        return
                    }

//                    const modalContent = document.querySelector(this.params.selector).cloneNode(true)
                    const modalContent = document.createElement('div')
                    modalContent.innerHTML = document.querySelector(this.params.selector).outerHTML

                    const actionSlot = modalContent.querySelector('[data-slot="action"]')

                    const contentNode = this.$el.querySelector('[ref="content"]')
                    const actionNode = this.$el.querySelector('[ref="action"]')

                    if (actionSlot) {
                        //actionSlot.outerHTML = actionSlot.innerHTML
                        actionNode.append(actionSlot)
                    }

                    modalContent.classList.remove('hidden')
                    contentNode.innerHTML = ''
                    contentNode.append(modalContent)
                })
            },
            prepareActionButton() {
                this.$nextTick(() => {
                    this.$el.querySelectorAll('[data-action]').forEach(button =>
                        button.addEventListener("click", () => this.params.callback(this.data, button.getAttribute('data-action')))
                    )
                })
            },
            backdrop: {
                ['@open-drawer.window']() {
                    this.params = this.$event.detail
                    this.data = this.params.data ?? {}
                    this.params.maxWidthClass = {
                        'sm': 'sm:max-w-sm',
                        'md': 'sm:max-w-md',
                        'lg': 'sm:max-w-lg',
                        'xl': 'sm:max-w-xl',
                        '2xl': 'sm:max-w-2xl',
                        '3xl': 'sm:max-w-3xl',
                        '4xl': 'sm:max-w-4xl'
                    }[this.params.size ? this.params.size : 'md']

                    this.open = true;
                },
                ['@update-drawer.window']() {
                    this.params = { ...this.params, ...this.$event.detail }
                    this.data = this.params.data ?? {}
                },
                ['@keydown.window.escape']() {
                    this.open = false
                },
            },
            contentArea: {
                ['@popstate.window']() {
                    this.open = false
                },
            },
        }
    }
</script>
