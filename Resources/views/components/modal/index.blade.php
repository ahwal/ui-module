@props(['maxWidth', 'xModel'])

@php
$maxWidth = [
    'sm' => 'sm:max-w-sm',
    'md' => 'sm:max-w-md',
    'lg' => 'sm:max-w-lg',
    'xl' => 'sm:max-w-xl',
    '2xl' => 'sm:max-w-2xl',
    '3xl' => 'sm:max-w-3xl',
    '4xl' => 'sm:max-w-4xl',
][$maxWidth ?? '2xl'];
@endphp

<div
    x-data="modal"
    x-init="() => {
        {{-- JS code is pushed once, so we need to do this for dynamic $xModel --}}
        $watch('{{ $xModel }}', val => show = val)

        {{-- ================================== --}}
        {{-- Inject function to x-data here, so we can use PHP variable $xModel --}}
        {{-- ================================== --}}
        $el._x_dataStack[0].setModel = value => {{ $xModel }} = value;
        $el._x_dataStack[0].getModel = () => {{ $xModel }}
    }"
    x-bind="binds.root"
    data-max-width="{{ $maxWidth }}"
    style="display: none;"
    {{ $attributes }}
>
    <div x-bind="binds.backdrop">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
    </div>

    <div x-bind="binds.content">{{ $slot }}</div>
</div>

@once
<script>
function modal() {
    return {
        show: false,
        maxWidth: '2xl',
        data: {}, title: null,
        init() {
            this.maxWidth = this.$el.getAttribute('data-max-width')

            this.$watch('show', show => {
                if (show) {
                    document.body.classList.add('overflow-y-hidden');
                    this.$nextTick(() => this.autoFocus());
                } else {
                    document.body.classList.remove('overflow-y-hidden');
                }
            });
        },
        binds: {
            root: {
                ['@close-modal.window']() {
                    this.setModel(this.show = false)
                },
                ['@close.stop']() {
                    this.$dispatch('close-modal')
                },
                ['@keydown.escape.window']() {
                    this.$dispatch('close-modal')
                },
                ['@keydown.tab.prevent']() {
                    this.$event.shiftKey || this.nextFocusable().focus()
                },
                ['@keydown.shift.tab.prevent']() {
                    this.prevFocusable().focus()
                },
                ['x-show']() {
                    return this.show
                },
                ['x-bind:class']() {
                    return "fixed inset-0 z-50 px-4 py-6 overflow-y-auto sm:px-0"
                },
            },
            backdrop: {
                ['x-show']() {
                    return this.show
                },
                ['x-bind:class']() {
                    return "fixed inset-0 transition-all transform"
                },
                ['@click']() {
                    this.$dispatch('close-modal')
                },
                ['x-transition:enter']() {
                    return "ease-out duration-300"
                },
                ['x-transition:enter-start']() {
                    return "opacity-0"
                },
                ['x-transition:enter-end']() {
                    return "opacity-100"
                },
                ['x-transition:leave']() {
                    return "ease-in duration-200"
                },
                ['x-transition:leave-start']() {
                    return "opacity-100"
                },
                ['x-transition:leave-end']() {
                    return "opacity-0"
                },
            },
            content: {
                ['x-show']() {
                    return this.show
                },
                ['x-bind:class']() {
                    return `mb-6 bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:w-full ${this.maxWidth} sm:mx-auto`
                },
                {{-- ['x-transition:enter']() {
                    return "ease-out duration-300"
                },
                ['x-transition:enter-start']() {
                    return "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                },
                ['x-transition:enter-end']() {
                    return "opacity-100 translate-y-0 sm:scale-100"
                },
                ['x-transition:leave']() {
                    return "ease-in duration-200"
                },
                ['x-transition:leave-start']() {
                    return "opacity-100 translate-y-0 sm:scale-100"
                },
                ['x-transition:leave-end']() {
                    return "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                }, --}}
            },
        },
        focusables() {
            // All focusable element types...
            let selector = 'a, button, input, textarea, select, details, [tabindex]:not([tabindex=\'-1\'])'

            return [...this.$el.querySelectorAll(selector)]
                // All non-disabled elements...
                .filter(el => ! el.hasAttribute('disabled'))
        },
        firstFocusable() { return this.focusables()[0] },
        lastFocusable() { return this.focusables().slice(-1)[0] },
        nextFocusable() { return this.focusables()[this.nextFocusableIndex()] || this.firstFocusable() },
        prevFocusable() { return this.focusables()[this.prevFocusableIndex()] || this.lastFocusable() },
        nextFocusableIndex() { return (this.focusables().indexOf(document.activeElement) + 1) % (this.focusables().length + 1) },
        prevFocusableIndex() { return Math.max(0, this.focusables().indexOf(document.activeElement)) -1 },
        autoFocus() { let focusable = this.$el.querySelector('[autofocus]'); if (focusable) focusable.focus() },
    }
}
</script>
@endonce
