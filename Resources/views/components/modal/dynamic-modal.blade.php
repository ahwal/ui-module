<div x-data="dynamicModal" x-bind="bindBackdrop" x-show="show"
    class="fixed inset-0 z-50 px-4 py-6 overflow-y-auto sm:px-0">

    <div x-show="show" x-bind="bindClose" class="fixed inset-0 transition-all transform">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
    </div>

    <div x-show="show" x-bind:class="params.maxWidthClass"
        x-transition:enter="ease-out duration-300"
        x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
        x-transition:leave="ease-in duration-200"
        x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
        x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        class="mb-6 transition-all transform bg-white shadow-xl sm:w-full sm:mx-auto">

        <form x-on:submit.prevent="submit">
            <template x-if="show">
                <div class="px-6 py-4">
                    <template x-if="params.title">
                        <div class="text-lg" x-text="params.title"></div>
                    </template>

                    <div class="mt-4" x-init="prepareModalContent"></div>
                </div>
            </template>

            <template x-if="show && !params.hideFooter">
                <div class="px-6 py-4 text-right bg-gray-100">
                    <x-ui::button.secondary x-bind="bindClose" x-text="params.cancelLabel ?? 'Batal'">
                    </x-ui::button.secondary>
                    <x-ui::button.primary type="submit" x-text="params.submitLabel ?? 'Simpan'"></x-ui::button.primary>
                </div>
            </template>
        </form>

    </div>
</div>


<script>
window.openDynamicModal = function (selector, params) {
    if (typeof params.callback == 'undefined') alert('Callback parameter required by openDynamicModal')

    window.dispatchEvent(
        new CustomEvent('open-dynamic-modal', {
            detail: { params: { selector, ...params } }
        })
    );
};

function dynamicModal() {
    return {
        show: false,
        params: {},
        data: {},
        errors: {},
        init() {
            this.$watch('show', show => {
                if (show) {
                    document.body.classList.add('overflow-y-hidden');
                    this.$nextTick(() => this.autoFocus());
                } else {
                    document.body.classList.remove('overflow-y-hidden');
                }
            });
        },
        prepareModalContent() {
            this.$nextTick(() => {
                if (!document.querySelector(this.params.selector)) {
                    console.log('Element for modal not found')
                    return
                }
                this.$el.innerHTML = document.querySelector(this.params.selector).outerHTML
            })
        },
        bindBackdrop: {
            ['@open-dynamic-modal.window']() {
                this.show = true
                this.params = this.$event.detail.params ?? {}
                this.data = this.params.data
                this.params.maxWidthClass = {
                    'sm': 'sm:max-w-sm',
                    'md': 'sm:max-w-md',
                    'lg': 'sm:max-w-lg',
                    'xl': 'sm:max-w-xl',
                    '2xl': 'sm:max-w-2xl',
                    '3xl': 'sm:max-w-3xl',
                    '4xl': 'sm:max-w-4xl'
                }[this.params.size ? this.params.size : 'md']
            },
            ['@update-modal.window']() {
                this.params = { ...this.params, ...this.$event.detail }
                this.data = this.params.data ?? {}
                this.errors = this.params.errors ?? []
            },
            ['@close-modal.window']() {
                this.params = {}
                this.data = {}
                this.errors = {}
                this.show = false
            },
            ['@close.stop']() {
                this.$dispatch('close-modal')
            },
            ['@keydown.escape.window']() {
                this.$dispatch('close-modal')
            },
            ['@keydown.tab.prevent']() {
                this.$event.shiftKey || this.nextFocusable().focus()
            },
            ['@keydown.shift.tab.prevent']() {
                this.prevFocusable().focus()
            },
        },
        bindClose: {
            ['@click']() {
                this.$dispatch('close-modal')
            },
            ['x-transition:enter']() {
                return 'ease-out duration-300'
            },
            ['x-transition:enter-start']() {
                return 'opacity-0'
            },
            ['x-transition:enter-end']() {
                return 'opacity-100'
            },
            ['x-transition:leave']() {
                return 'ease-in duration-200'
            },
            ['x-transition:leave-start']() {
                return 'opacity-100'
            },
            ['x-transition:leave-end']() {
                return 'opacity-0'
            },
        },
        async submit() {
            if (await this.params.callback(this.data)) this.$dispatch('close-modal')
        },
        focusables() {
            // All focusable element types...
            let selector = 'a, button, input, textarea, select, details, [contenteditable], [tabindex]:not([tabindex=\'-1\'])'

            return [...this.$el.querySelectorAll(selector)]
                // All non-disabled elements...
                .filter(el => !el.hasAttribute('disabled'))
        },
        firstFocusable() {
            return this.focusables()[0]
        },
        lastFocusable() {
            return this.focusables().slice(-1)[0]
        },
        nextFocusable() {
            return this.focusables()[this.nextFocusableIndex()] || this.firstFocusable()
        },
        prevFocusable() {
            return this.focusables()[this.prevFocusableIndex()] || this.lastFocusable()
        },
        nextFocusableIndex() {
            return (this.focusables().indexOf(document.activeElement) + 1) % (this.focusables().length + 1)
        },
        prevFocusableIndex() {
            return Math.max(0, this.focusables().indexOf(document.activeElement)) - 1
        },
        autoFocus() {
            let focusable = this.$el.querySelector('[autofocus]');
            if (focusable) focusable.focus()
        },
    }
}
</script>
