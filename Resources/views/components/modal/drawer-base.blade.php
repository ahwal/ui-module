@props(['xModel', 'maxWidth', 'header' => '', 'footer' => ''])

@php
$maxWidth = [
    'sm' => 'sm:max-w-sm',
    'md' => 'sm:max-w-md',
    'lg' => 'sm:max-w-lg',
    'xl' => 'sm:max-w-xl',
    '2xl' => 'sm:max-w-2xl',
    '3xl' => 'sm:max-w-3xl',
    '4xl' => 'sm:max-w-4xl',
][$maxWidth ?? '3xl'];
@endphp

<div
    {{-- x-data="drawerBase" --}}
    x-on:keydown.window.escape="{{ $xModel }} = false"
    x-show="{{ $xModel }}"
    x-effect="() => {
        if ({{ $xModel }}) {
            $nextTick(() => {
                let autoFocus = $el.querySelector('[autofocus]')
                if (autoFocus) autoFocus.focus()
            })
        }
    }"
    class="fixed inset-0 z-40 overflow-hidden"
    aria-labelledby="drawer-title" aria-modal="true"
>
    <div class="absolute inset-0 overflow-hidden">

        <div x-show="{{ $xModel }}" x-on:click="{{ $xModel }} = false"
            x-transition:enter="ease-in-out duration-500"
            x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100"
            x-transition:leave="ease-in-out duration-500"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            class="absolute inset-0 transition-opacity bg-gray-500 bg-opacity-75"
            aria-hidden="true"></div>

        <div class="fixed inset-y-0 right-0 flex max-w-full pl-10">

            <div x-show="{{ $xModel }}" x-transition:enter="transform transition ease-in-out duration-500 sm:duration-700"
                x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0"
                x-transition:leave="transform transition ease-in-out duration-500 sm:duration-700"
                x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full"
                class="relative w-screen {{ $maxWidth }}">

                <div x-show="{{ $xModel }}" x-transition:enter="ease-in-out duration-500" x-transition:enter-start="opacity-0"
                    x-transition:enter-end="opacity-100" x-transition:leave="ease-in-out duration-500"
                    x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
                    class="absolute top-0 left-0 flex pt-4 pr-2 -ml-8 sm:-ml-10 sm:pr-4">
                    <button
                        class="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                        x-on:click="{{ $xModel }} = false"
                        x-ref="closeButton"
                    >
                        <span class="sr-only">Close panel</span>
                        <svg class="w-6 h-6" x-description="Heroicon name: outline/x" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </button>
                </div>

                <template x-if="{{ $xModel }}">
                    <div class="grid h-screen bg-white shadow-xl">
                        @if ($header !== '')
                        <div class="flex px-4 py-4 border-b sm:px-6">
                            {{ $header }}
                        </div>
                        @endif
                        <div class="overflow-y-auto">
                            {{ $slot }}
                        </div>
                        @if ($footer !== '')
                        <div class="flex px-4 py-4 border-t sm:px-6">
                            {{ $footer }}
                        </div>
                        @endif
                    </div>
                </template>
            </div>

        </div>
    </div>
</div>

{{-- @once
<script>
function drawerBase() {
    return {
        init() {
            this.$watch(this.$el.getAttribute('x-show'), val => {
                if (val) this.$nextTick(() => this.autoFocus());
            })
        },
        focusables() {
            // All focusable element types...
            let selector = 'a, button, input, textarea, select, details, [contenteditable], [tabindex]:not([tabindex=\'-1\'])'

            return [...this.$el.querySelectorAll(selector)]
                // All non-disabled elements...
                .filter(el => ! el.hasAttribute('disabled'))
        },
        firstFocusable() { return this.focusables()[0] },
        lastFocusable() { return this.focusables().slice(-1)[0] },
        nextFocusable() { return this.focusables()[this.nextFocusableIndex()] || this.firstFocusable() },
        prevFocusable() { return this.focusables()[this.prevFocusableIndex()] || this.lastFocusable() },
        nextFocusableIndex() { return (this.focusables().indexOf(document.activeElement) + 1) % (this.focusables().length + 1) },
        prevFocusableIndex() { return Math.max(0, this.focusables().indexOf(document.activeElement)) -1 },
        autoFocus() { let focusable = this.$el.querySelector('[autofocus]'); if (focusable) focusable.focus() },
    }
}
</script>
@endonce --}}
