<div class="hidden"><div id="confirmContent"></div></div>

<script>
window.openConfirm = function (message, title) {
    document.querySelector('#confirmContent').innerHTML = message
    return new Promise(function (resolve, reject) {
        openDynamicModal('#confirmContent', {
            title: title ?? 'Anda Yakin?',
            submitLabel: 'Ya',
            cancelLabel: 'Tidak',
            callback: () => resolve(true) || true
        })
    });
};
</script>
