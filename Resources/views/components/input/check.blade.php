@props([
    'xModel',
    'options' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'orientation' => 'vertical',
    'bindClass' => null,
])

@php
    if (is_string($options)) $options = json_decode(htmlspecialchars_decode($options));
@endphp

<div
    x-data='{ options: @json($options) }'
    x-init="options = $root.dataset.options ? JSON.parse($root.dataset.options) : options"
    class="mt-2 {{ $orientation == 'horizontal' ? 'flex space-x-3' : 'space-y-2' }}"
    {!! $attributes !!}>
    <template x-for="key in Object.keys(options)" :key="key" hidden>
        <div class="relative flex items-start">
            <div class="flex items-center h-5">
                <input
                    type="checkbox"
                    x-bind:id="'checkbox-' + key"
                    x-bind:name="key"
                    x-bind:value="key"
                    x-model="{{ $xModel }}"
                    class="w-4 h-4 text-green-600 border-gray-300 rounded focus:ring-green-500 focus:ring-opacity-50"
                    @if ($bindClass)
                        x-bind:class="{!! htmlspecialchars_decode($bindClass) !!}"
                    @endif
                >
            </div>
            <div class="ml-2 text-sm">
                <label x-bind:for="'checkbox-' + key"
                    class="font-medium text-gray-700 cursor-pointer"
                    x-text="options[key]"
                ></label>
            </div>
        </div>
    </template>
</div>
