@props(['xModel'])

@php
    $id = Illuminate\Support\Str::random(10);
@endphp
<div class="flex items-center"
    x-data="{ on: false }"
    x-effect="$dispatch('change', on)"
    x-modelable="on"
    x-model="{{ $xModel }}"
    {!! $attributes !!}
>
    <button type="button"
        class="relative inline-flex flex-shrink-0 w-8 h-4 transition-colors duration-200 ease-in-out bg-green-600 border-2 border-transparent rounded-full cursor-pointer focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
        role="switch"
        aria-checked="true"
        x-ref="switch"
        x-state:on="Enabled"
        x-state:off="Not Enabled"
        x-bind:class="{ 'bg-green-600': on, 'bg-gray-200': !(on) }"
        aria-labelledby="{{ $id }}"
        x-bind:aria-checked="on ? 'true' : 'false'"
        x-on:click="on = !on"
    >
        <span aria-hidden="true" class="inline-block w-3 h-3 transition duration-200 ease-in-out transform translate-x-4 bg-white rounded-full shadow pointer-events-none ring-0"
            x-state:on="Enabled"
            x-state:off="Not Enabled"
            x-bind:class="{ 'translate-x-4': on, 'translate-x-0': !(on) }"
        ></span>
    </button>
    <span class="ml-2 cursor-pointer" id="{{ $id }}" x-on:click="on = !on; $refs.switch.focus()">
        {{ $slot }}
    </span>
</div>
