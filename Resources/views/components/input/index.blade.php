<input
    {{ $attributes->merge([
        'type' => 'text',
        'class' => 'py-1 text-sm
            border-gray-300 rounded-md shadow-sm
            focus:border-green-500 focus:ring focus:ring-green-500 focus:ring-opacity-50'
    ]) }}
/>
