@props([
    'xModel',
    'options' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'placeholder' => '-- Pilih --',
    'bindClass' => null,
])

@php
    if (is_string($options)) $options = json_decode(htmlspecialchars_decode($options));
@endphp

<select
    x-model="{{ $xModel }}"
    {{ $attributes->merge([
        'class' => "block py-1 pl-3 pr-10 text-base border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50 sm:text-sm"
    ]) }}
    @if ($bindClass)
        x-bind:class="{!! htmlspecialchars_decode($bindClass) !!}"
    @endif
>
    @if ($slot->isNotEmpty())
        {{ $slot }}
    @else
        <option value="">{{ $placeholder }}</option>
        @foreach ($options as $value => $label)
        <option value="{{ $value }}">{{ $label }}</option>
        @endforeach
    @endif
</select>
