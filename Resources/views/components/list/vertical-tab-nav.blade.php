@props(['items' => '{}', 'xInit' => ''])

<div
    x-data="{
        value: '',
        items: {!! $items !!},
        reloadItems($root) {
            this.items = $root.dataset.items ? JSON.parse($root.dataset.items) : this.items
        },
    }"
    x-init="items = $root.dataset.items ? JSON.parse($root.dataset.items) : items; {{ $xInit }}"
    x-effect="$dispatch('change', value);"
    x-modelable="value"
    {{ $attributes }}
>
    <div class="lg:hidden" x-id="['tab-select']">
        <label :for="$id('tab-select')" class="sr-only">Pilih tab</label>
        <select
            :id="$id('tab-select')"
            :name="$id('tab-select')"
            class="block w-full border-gray-300 rounded-md focus:ring-green-500 focus:border-green-500 focus:ring-opacity-50"
            x-model="value"
        >
            <template x-for="(item, name) in items" hidden>
                <option x-bind:value="name" x-text="item"></option>
            </template>
        </select>
    </div>
    <aside class="hidden px-2 py-6 lg:block sm:px-6 lg:py-0 lg:px-0">
        <nav>
            <template x-for="(item, name) in items" :key="name" hidden>
                <div x-on:click.prevent="value = name" class="cursor-pointer">
                    @if ($slot->isNotEmpty())
                        {{ $slot }}
                    @else
                        <a href="#"
                            class="flex items-center px-3 py-2 text-sm font-medium group"
                            x-bind:class="{
                                'text-gray-900 hover:text-gray-900 hover:bg-gray-100': name != value,
                                'text-green-700 bg-gray-50 hover:text-green-700': name == value,
                            }">
                            <span class="truncate" x-text="item"></span>
                        </a>
                    @endif
                </div>
            </template>
        </nav>
    </aside>
</div>
