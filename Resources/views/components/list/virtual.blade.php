@props(['footer' => '', 'xList' => 'data', 'listClass' => ''])

<div
    x-data="virtualList"
    x-bind="viewport"
    x-on:click="draw" {{-- used to force list redraw, because sometimes list is not visible --}}
    x-effect="list.visibleData = prepareData({{ $xList }}, (item, idx) => {
        return { index: idx, data: item }
    });"
    data-var="{{ $xList }}"
    {{ $attributes
        ->merge(['class' => 'w-full overflow-y-auto'])
        ->filter(fn ($value, $key) => ! preg_match('/(x-for|x-bind:key)/', $key))
    }}
>
    <div x-show="!{{ $xList }}.length" class="grid w-full h-full text-center bg-gray-200 place-content-center">
        Tidak ada data
    </div>

    <div x-show="{{ $xList }}.length" class="w-full js-content {{ $listClass }}" x-ref="content" :style="contentStyle()" x-on:click.stop>
        <template {{ $attributes->filter(fn ($value, $key) => preg_match('/(x-for|x-bind:key)/', $key)) }} hidden>
            {{ $slot }}
        </template>
    </div>

    <div>
        {{ $footer }}
    </div>
</div>


@once
<script>
function virtualList() {
    return {
        list: {},
        heightMap: [],
        viewport: {
            ['@scroll']() {
                this.draw();
            },
            ['@resize.window']() {
                this.$dispatch('data-loaded');
            },
            ['@data-redraw.window']() {
                this.draw();
            },
            ['@data-loaded.window']() {
                this.calculate();
            },
            ['@data-reset.window']() {
                this.resetScroll();
            },
        },
        init() {
            this.list = {
                visibleData: [],
                dataLength: 0,
                start: 0,
                end: 0,
                topPos: 0,
                btmPos: 0,
                avgHeight: 0,
            };

            if (this.$el.getAttribute('data-var')) {
                this.$watch(
                    this.$el.getAttribute('data-var'),
                    (newVal, oldVal) => {
                        if (newVal.length == 0) {
                            this.resetScroll();
                        }
                    }
                );
            }

            this.$dispatch('scroll-end');
        },
        prepareData(data, mappingCallback) {
            this.list.dataLength = data.length;
            return data
                .slice(this.list.start, this.list.end)
                .map((item, index) => {
                    return mappingCallback(item, index + this.list.start);
                });
        },
        contentStyle() {
            return `
            padding-top: ${this.list.topPos}px;
            padding-bottom: ${this.list.btmPos}px;
        `;
        },
        rows(index) {
            // $refs is not used because sometimes it give errors on turbolink nav

            if (!this.$el.querySelector('.js-content')) return [];

            const rows = this.$el
                .querySelector('.js-content')
                .querySelectorAll(':scope >:not(template)');

            if (typeof index != 'undefined') return rows[index];

            return rows;
        },

        resetScroll() {
            // make sure scroll is on base position
            this.list.start =
                this.list.end =
                this.list.topPos =
                this.list.btmPos =
                this.list.avgHeight =
                    0;
            this.$nextTick(() => this.draw());
        },

        async calculate() {
            await this.$nextTick((i) => i);

            let content_height = this.list.topPos - this.$el.scrollTop;

            let i = this.list.start;

            while (
                content_height < this.$el.offsetHeight &&
                i < this.list.dataLength
            ) {
                let row = this.rows(i - this.list.start);

                if (!row) {
                    this.list.end = i + 1;
                    await this.$nextTick((i) => i); // render the newly visible row

                    row = this.rows(i - this.list.start);
                }

                const row_height = (this.heightMap[i] = row.offsetHeight);
                content_height += row_height;
                i += 1;
            }

            this.list.end = i;

            const remaining = this.list.dataLength - this.list.end;
            this.list.avgHeight =
                (this.list.topPos + content_height) / this.list.end || 0;

            this.list.btmPos = parseInt(remaining * this.list.avgHeight);
            this.heightMap.length = this.list.dataLength;
        },
        async draw() {
            if (
                this.$el.offsetHeight + this.$el.scrollTop >=
                this.$el.scrollHeight
            ) {
                this.$dispatch('scroll-end');
            }

            const old_start = this.list.start;

            for (let v = 0; v < this.rows().length; v += 1) {
                this.heightMap[this.list.start + v] = this.rows(v).offsetHeight;
            }

            let i = 0;
            let y = 0;

            while (i < this.list.dataLength) {
                const row_height = this.heightMap[i] || this.list.avgHeight;
                if (y + row_height > this.$el.scrollTop) {
                    this.list.start = i;
                    this.list.topPos = y;

                    break;
                }

                y += row_height;
                i += 1;
            }

            while (i < this.list.dataLength) {
                y += this.heightMap[i] || this.list.avgHeight;
                i += 1;

                if (y > this.$el.scrollTop + this.$el.offsetHeight) break;
            }

            this.list.end = i;

            const remaining = this.list.dataLength - this.list.end;
            this.list.avgHeight = y / this.list.end || 0;

            while (i < this.list.dataLength)
                this.heightMap[i++] = this.list.avgHeight;

            this.list.btmPos = parseInt(remaining * this.list.avgHeight);

            // prevent jumping if we scrolled up into unknown territory
            if (this.list.start < old_start) {
                await this.$nextTick((i) => i); // use dummy function

                let expected_height = 0;
                let actual_height = 0;

                for (let i = this.list.start; i < old_start; i += 1) {
                    if (this.rows(i - this.list.start)) {
                        expected_height += this.heightMap[i];
                        actual_height += this.rows(
                            i - this.list.start
                        ).offsetHeight;
                    }
                }

                this.$el.scrollTo(
                    0,
                    this.$el.scrollTop + (actual_height - expected_height)
                );
            }
        },
    }
}
</script>
@endonce
