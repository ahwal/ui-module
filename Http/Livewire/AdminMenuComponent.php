<?php

namespace Modules\UI\Http\Livewire;

use Livewire\Component;
use Modules\UI\Actions\GetAdminMenuAction;
use Modules\UI\Navigation\NavigationItem;

class AdminMenuComponent extends Component
{
    protected $listeners = [
        'refresh-navigation-menu' => '$refresh',
        'refresh-menu' => '$refresh',
    ];

    /** @var NavigationItem[] $items */
    public $items = [];

    public function mount()
    {
        $this->items = GetAdminMenuAction::run();
    }

    public function render()
    {
        return view('ui::livewire.admin-menu-component', [
            'title' => $this->title,
            'submenu' => $this->submenu,
        ]);
    }

    public function getTitleProperty()
    {
        return $this->items[session('current_module')]->getLabel() ?? session('current_module');
    }

    public function getSubmenuProperty()
    {
        return $this->items[session('current_module')]->getSubmenu() ?? [];
    }
}
