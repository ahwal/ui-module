<?php

namespace Modules\UI\Http\Livewire;

use Livewire\Component;

class PortalMenuComponent extends Component
{
    protected $listeners = [
        'refresh-navigation-menu' => '$refresh',
        'refresh-menu' => '$refresh',
    ];

    public function render()
    {
        return view('ui::livewire.portal-menu-component');
    }
}
