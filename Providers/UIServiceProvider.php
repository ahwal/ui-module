<?php

namespace Modules\UI\Providers;

use Illuminate\Support\ServiceProvider;
use Livewire\Component;

class UIServiceProvider extends ServiceProvider
{
    protected $moduleName = 'UI';
    protected $moduleNameLower = 'ui';

    public function boot()
    {
        $this->registerViews();
    }

    public function register()
    {
        Component::macro('notify', function ($message, $type = 'success') {
            $this->dispatchBrowserEvent('notify', [
                'message' => $message,
                'type' => $type,
            ]);
        });
    }

    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
