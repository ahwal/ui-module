<?php

namespace Modules\UI\Navigation;

use Closure;
use Illuminate\Support\Facades\Route;

class NavigationItem
{
    protected ?Closure $isActiveWhen = null;

    protected ?string $label;

    protected ?string $route = null;

    protected ?string $link = null;

    protected ?array $submenu = null;

    final public function __construct()
    {
    }

    public static function make(): static
    {
        return app(static::class);
    }

    public function isActiveWhen(Closure $callback): static
    {
        $this->isActiveWhen = $callback;

        return $this;
    }

    public function label(?string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function route(?string $route): static
    {
        $this->route = $route;

        return $this;
    }

    public function link(?string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function submenu(?array $submenu): static
    {
        $this->submenu = $submenu;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getUrl(): string
    {
        return $this->route && Route::has($this->route)
            ? route($this->route)
            : ($this->link ?? '#');
    }

    public function getSubmenu(): ?array
    {
        return $this->submenu;
    }

    public function isSeparator(): bool
    {
        return $this->label == null;
    }

    public function isActive(): bool
    {
        $callback = $this->isActiveWhen;

        if ($callback === null) {
            return false;
        }

        return $callback($this);
    }
}
